const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}




//Q1. Find all the movies with total earnings more than $500M. 

function earningsOver500() {
    const Over500M = Object.entries(favouritesMovies).reduce((result, movie) => {

        const earningInNumber = Number(movie[1].totalEarnings.replace('$', "").replace('M', ""));
        if (earningInNumber > 500) {
            result[movie[0]] = { ...movie[1] };
        }

        return result;
    }, {});

    console.log(Over500M);
}

earningsOver500();


//Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

function moviesOver3OscarN500M() {
    return Object.entries(favouritesMovies).reduce((result, movie) => {

        const earningInNumber = Number(movie[1].totalEarnings.replace('$', "").replace('M', ""));
        if (earningInNumber > 500 && movie[1].oscarNominations > 3) {
            result[movie[0]] = { ...movie[1] };
        }

        return result;
    }, {});

}

console.log("2- ", moviesOver3OscarN500M());

//Q.3 Find all movies of the actor "Leonardo Dicaprio".

function moviesWithDicaprio() {
    return Object.entries(favouritesMovies).reduce((DicaprioMovies, currMovie) => {
        if (currMovie[1].actors.includes("Leonardo Dicaprio")) {
            DicaprioMovies[currMovie[0]] = { ...currMovie[1] };
        }
        return DicaprioMovies;
    }, {})
}

console.log("3- Dicaprio movies- ", moviesWithDicaprio());

//Q.4 Sort movies (based on IMDB rating)
// if IMDB ratings are same, compare totalEarning as the secondary metric.

function moviesSorted() {
    return Object.entries(favouritesMovies).sort((movie1, movie2) => {
        const diff = movie1[1].imdbRating - movie2[1].imdbRating;
        if (diff == 0) {
            return (Number(movie1[1].totalEarnings.replace('$', "").replace('M', "")) -
                Number(movie2[1].totalEarnings.replace('$', "").replace('M', "")));
        }
        else {
            return diff;
        }
    }).reduce((movieObject, currMovie) => {
        movieObject[currMovie[0]] = { ...currMovie[1] };
        return movieObject
    }, {});
}

console.log("4- sorted movies- ", moviesSorted());


//Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
// drama > sci-fi > adventure > thriller > crime

function movieGrouping() {
    const genreOrder = { "drama": 5, "sci-fi": 4, "adventure": 3, "thriller": 2, "crime": 1 };
    return Object.entries(favouritesMovies).reduce((genreGrouped, currMovie) => {
        let currGenre = currMovie[1].genre;

        if (currGenre.length > 1) {
            currGenre = currMovie[1].genre.sort((genre1, genre2) => {
                return genreOrder[genre2] - genreOrder[genre1];
            })
        }

        console.log(currGenre);
        if (genreGrouped[currGenre[0]]) {
            genreGrouped[currGenre[0]].push({ [currMovie[0]]: { ...currMovie[1] } });
        }
        else {
            genreGrouped[currGenre[0]] = [{ [currMovie[0]]: { ...currMovie[1] } }] 
        }

        return genreGrouped;
    }, {});
}

console.log("5- movies grouped on genre- ", JSON.stringify(movieGrouping(),null,2));